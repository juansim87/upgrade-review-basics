//**Iteración #1: Mix for e includes** Dado el siguiente javascript usa forof para recorrer el array de películas, genera un nuevo array con las categorías de las películas 
//e imprime por consola el array de categorías. Ten en cuenta que las categorías no deberían repetirse. Para filtrar las categorías puedes ayudarte de la función **.includes()**
// const movies = [
//     {title: 'Madaraspar', duration: 192, categories: ['comedia', 'aventura']},
//     {title: 'Spiderpan', duration: 122, categories: ['aventura', 'acción']},
//     {title: 'Solo en Whatsapp', duration: 223, categories: ['comedia', 'thriller']},
//     {title: 'El gato con guantes', duration: 111, categories: ['comedia', 'aventura', 'animación']},
// ]

// const myCategories = [];

// for (let movie = 0; movie < movies.length; movie++) {
//   for (let info = 0; info < movies[movie].categories.length; info++) {
//     let movieCategory = movies[movie].categories[info];
// if (myCategories.includes(movieCategory) == false) {
//   myCategories.push(movieCategory);
// }
//   }
// }

// for (const movie of movies) {
//     movie.categories.forEach((info) => {
//         if (myCategories.includes(info) == false) {
//             myCategories.push(info);
//     }
    
// }
// )
// }
// console.log(myCategories)

    // 


//**Iteración #2: Mix Fors** Dado el siguiente javascript usa forof y forin para hacer la media del volumen 
//de todos los sonidos favoritos que tienen los usuarios.

const users = [
    {array: 'Manolo el del bombo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 50},
            rain: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {array: 'Mortadelo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 30},
            shower: {format: 'ogg', volume: 55},
            train: {format: 'mp3', volume: 60},
        }
    },
    {array: 'Super Lopez',
        favoritesSounds: {
            shower: {format: 'mp3', volume: 50},
            train: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {array: 'El culebra',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 67},
            wind: {format: 'ogg', volume: 35},
            firecamp: {format: 'mp3', volume: 60},
        }
    },
]

let soundCount = 0;
let soundSum = 0;
for (const info of users) {

    for (const sounds in info.favoritesSounds) {
        soundSum += info.favoritesSounds[sounds].volume;
        soundCount++;
    }
        }
            
      let media = soundSum/soundCount;

      console.log(media);
    





//Iteración 4


// let continents = ["America", "Asia", "Europa", "Africa", "Oceania"];

// function findArrayIndex(continent, text) {
//   if (continents.includes(text)) {
//     return Posicion ${continent.indexOf(text)};
//   }
// }

// console.log(findArrayIndex(continents, "Africa"));


    // //Iteración 5 Dado
    // function rollDice(dice) {
    //     return Math.floor(Math.random(dice) * 6) + 1;
    // };
    
    // //para varias tiradas>>añadir elementos al array
    // const tiradas = [1, 1];
    // tiradas.forEach(function () {
    // console.log(rollDice(1));
    // });




    /* **Iteración #6: Función swap**

Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. La función deberá intercambiar la posición de los 
valores de los indices que hayamos enviado como parametro. Retorna el array resultante.

Sugerencia de array:*/

// let jugadoresMal = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño', 'Kangrim Bienzema'];


// function swap(array, a, b) {
// [array[a], array[b]] = [array[b], array[a]];
// return array;
    
// }

// console.log(swap(jugadoresMal, 0, 4));